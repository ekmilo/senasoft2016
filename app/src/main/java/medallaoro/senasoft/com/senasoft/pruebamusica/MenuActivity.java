package medallaoro.senasoft.com.senasoft.pruebamusica;

import android.content.Intent;
import android.content.res.Configuration;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;

import java.util.ArrayList;
import java.util.List;

import medallaoro.senasoft.com.senasoft.R;
import medallaoro.senasoft.com.senasoft.pruebamusica.fragments.AuthorFragment;
import medallaoro.senasoft.com.senasoft.pruebamusica.fragments.FragmentAuthorList;
import medallaoro.senasoft.com.senasoft.pruebamusica.utitilies.Constantes;
import medallaoro.senasoft.com.senasoft.pruebamusica.utitilies.ReadFiles;

public class MenuActivity extends AppCompatActivity  implements SensorEventListener {

    List<Integer>listIds;
    private SensorManager mSensorManager;
    private Sensor mSensor;
    private int id=0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu);
        mSensorManager = (SensorManager) getSystemService(SENSOR_SERVICE);
        mSensor = mSensorManager.getDefaultSensor(Sensor.TYPE_PROXIMITY);

        List<String> categorias= ReadFiles.readFile(MenuActivity.this, Constantes.name_file);
        categorias.add(getResources().getString(R.string.author));
        Toolbar toolbar = (Toolbar)findViewById(R.id.toolbar);
        listIds=new ArrayList<>();
        int id=0;
        LinearLayout linearLayout=new LinearLayout(MenuActivity.this);
        int orientation= getResources().getConfiguration().orientation;
        if(orientation == Configuration.ORIENTATION_LANDSCAPE){
            linearLayout.setOrientation(LinearLayout.VERTICAL);
        }else{
            linearLayout.setOrientation(LinearLayout.HORIZONTAL);
        }
        for(String text : categorias){
            Button button=new Button(MenuActivity.this);
            id++;
            button.setId(id);
            button.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT,
                    ViewGroup.LayoutParams.MATCH_PARENT));
            button.setBackground(getResources().getDrawable(R.drawable.button_selector));
            button.setTextColor(getResources().getColor(android.R.color.white));
            button.setText(text);
            button.setOnClickListener(click());
            listIds.add(button.getId());
            linearLayout.addView(button);
        }
        toolbar.addView(linearLayout);
    }

    private View.OnClickListener click(){
        return new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               select(v);
            }
        };
    }
    private void select(View v){
        for (Integer id : listIds) {
            if (id.equals(v.getId())) {
                v.setSelected(true);
            } else {
                findViewById(id).setSelected(false);
            }
        }
        selectOption(v.getId());
    }
    private void selectOption(Integer id){
        Fragment fragment;
        if(id!= 5){
            FragmentAuthorList fragmentAuthorList=new FragmentAuthorList();
            Bundle bundle = new Bundle();
            bundle.putInt(Constantes.key,id);
            fragmentAuthorList.setArguments(bundle);
           fragment=fragmentAuthorList;
        }else{
            fragment=new AuthorFragment();
        }

        FragmentTransaction tx= getSupportFragmentManager().beginTransaction();
        tx.replace(R.id.contenedor,fragment);
        tx.commit();


    }


    protected void onResume() {
        super.onResume();
        mSensorManager.registerListener(this, mSensor,
                SensorManager.SENSOR_DELAY_NORMAL);
    }

    protected void onPause() {
        super.onPause();
        mSensorManager.unregisterListener(this);
    }

    public void onAccuracyChanged(Sensor sensor, int accuracy) {
    }

    public void onSensorChanged(SensorEvent event) {
        if (event.values[0] == 0) {
            id++;
            if(id>5) {
                id = 1;
            }
            View v=findViewById(id);
            select(v);
        }
    }
}
