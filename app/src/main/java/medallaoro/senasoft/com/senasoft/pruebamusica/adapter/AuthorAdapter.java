package medallaoro.senasoft.com.senasoft.pruebamusica.adapter;

import android.content.Context;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import medallaoro.senasoft.com.senasoft.R;
import medallaoro.senasoft.com.senasoft.pruebamusica.entity.Author;
import medallaoro.senasoft.com.senasoft.pruebamusica.utitilies.Constantes;

/**
 * Created by ekmil on 13/7/2016.
 */
public class AuthorAdapter extends RecyclerView.Adapter<AuthorAdapter.AuthorViewHolder>   {

    List<Author> authorlist;
    Context context;
    public AuthorAdapter(List<Author> authorlist, Context context){
        this.authorlist = authorlist;
        this.context=context;
    }

    public static class AuthorViewHolder extends RecyclerView.ViewHolder{

        TextView textView;
        ImageView imageView;
        Spinner spinner;

        public AuthorViewHolder(View itemView) {
            super(itemView);
            textView=(TextView)itemView.findViewById(R.id.textview);
            imageView=(ImageView)itemView.findViewById(R.id.imageview);
            spinner=(Spinner) itemView.findViewById(R.id.spinner);
        }
    }


    @Override
    public AuthorViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v= LayoutInflater.from(parent.getContext()).inflate(R.layout.author_item,parent,false);
        AuthorViewHolder authorViewHolder = new AuthorViewHolder(v);
        return authorViewHolder;
    }

    @Override
    public void onBindViewHolder(AuthorViewHolder holder, int position) {
        holder.textView.setText(authorlist.get(position).getName());
        holder.imageView.setImageResource(authorlist.get(position).getImage());
        holder.spinner.setAdapter(new ArrayAdapter<String>(context,android.R.layout.simple_dropdown_item_1line, Constantes.votacion));
        holder.spinner.setSelection(authorlist.get(position).getVote());
    }

    @Override
    public int getItemCount() {
        return authorlist.size();
    }


}
