package medallaoro.senasoft.com.senasoft.pruebamusica.fragments;


import android.content.Context;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;

import medallaoro.senasoft.com.senasoft.R;
import medallaoro.senasoft.com.senasoft.pruebamusica.entity.Author;
import medallaoro.senasoft.com.senasoft.pruebamusica.utitilies.Constantes;
import medallaoro.senasoft.com.senasoft.pruebamusica.utitilies.ReadFiles;

/**
 * A simple {@link Fragment} subclass.
 */
public class AuthorFragment extends Fragment {


    public AuthorFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        final View view = inflater.inflate(R.layout.fragment_author, container, false);
        Button button=(Button)view.findViewById(R.id.raised);
        final EditText editText=(EditText)view.findViewById(R.id.edittext);
        final Spinner categoria=(Spinner)view.findViewById(R.id.categoria);
        final Spinner voto=(Spinner)view.findViewById(R.id.voto);
        categoria.setAdapter(new ArrayAdapter<String>(getContext(),android.R.layout.simple_dropdown_item_1line,
                ReadFiles.readFile(getContext(), Constantes.name_file)));
        voto.setAdapter(new ArrayAdapter<String>(getContext(),android.R.layout.simple_dropdown_item_1line,
                Constantes.votacion));
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Author author=new Author(editText.getText().toString(),categoria.getSelectedItem().toString(),
                        Integer.valueOf(voto.getSelectedItem().toString()),R.drawable.music);
                switch(categoria.getSelectedItemPosition()){
                    case 0:
                        Constantes.autores1.add(author);
                        break;
                    case 1:
                        Constantes.autores2.add(author);
                        break;
                    case 2:
                        Constantes.autores3.add(author);
                        break;
                    case 3:
                        Constantes.autores4.add(author);
                        break;
                }
                Snackbar.make(view,R.string.author,Snackbar.LENGTH_LONG).show();
            }
        });

        return view;
    }

}
