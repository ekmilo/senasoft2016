package medallaoro.senasoft.com.senasoft.pruebamusica.entity;

/**
 * Created by ekmil on 13/7/2016.
 */
public class Author {
    private String name,category;
    private int vote;
    private int image;

    public Author(String name, String category, int vote, int image) {
        this.name = name;
        this.category = category;
        this.vote = vote;
        this.image = image;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public int getVote() {
        return vote;
    }

    public void setVote(int vote) {
        this.vote = vote;
    }

    public int getImage() {
        return image;
    }

    public void setImage(int image) {
        this.image = image;
    }
}
