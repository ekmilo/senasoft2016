package medallaoro.senasoft.com.senasoft.pruebamusica.fragments;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import medallaoro.senasoft.com.senasoft.R;
import medallaoro.senasoft.com.senasoft.pruebamusica.adapter.AuthorAdapter;
import medallaoro.senasoft.com.senasoft.pruebamusica.entity.Author;
import medallaoro.senasoft.com.senasoft.pruebamusica.utitilies.Constantes;

/**
 * A simple {@link Fragment} subclass.
 */
public class FragmentAuthorList extends Fragment {

    private int id;
    public FragmentAuthorList() {
        // Required empty public constructor
    }

    @Override
    public void setArguments(Bundle args) {
        super.setArguments(args);
        id = args.getInt(Constantes.key);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_fragment_author_list, container, false);
        RecyclerView recyclerView=(RecyclerView)v.findViewById(R.id.recyclerview);
        recyclerView.setLayoutManager(new GridLayoutManager(getContext(),2));
        List<Author>authorList;
        AuthorAdapter authorAdapter;
        switch (id){
            case 1:
                authorList=Constantes.autores1;
                break;
            case 2:
                authorList=Constantes.autores2;
                break;
            case 3:
                authorList=Constantes.autores3;
                break;
            case 4:
                authorList=Constantes.autores4;
                break;
            default:
                authorList=Constantes.autores1;
                break;
        }
        authorAdapter =new AuthorAdapter(authorList,getContext());
        recyclerView.setAdapter(authorAdapter);
        return v;
    }

}
