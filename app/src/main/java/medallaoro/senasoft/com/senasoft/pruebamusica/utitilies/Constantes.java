package medallaoro.senasoft.com.senasoft.pruebamusica.utitilies;

import java.util.ArrayList;
import java.util.List;

import medallaoro.senasoft.com.senasoft.R;
import medallaoro.senasoft.com.senasoft.pruebamusica.entity.Author;

/**
 * Created by ekmil on 13/7/2016.
 */
public class Constantes {
    public static String name_file="categorias.csv";
    public static List<Author> autores1 = new ArrayList<>();
    public static List<Author> autores2 = new ArrayList<>();
    public static List<Author> autores3 = new ArrayList<>();
    public static List<Author> autores4 = new ArrayList<>();
    static{
        autores1.add(new Author("carlos","1",0, R.drawable.music));
        autores1.add(new Author("carloasdfas","2",0, R.drawable.music));
        autores2.add(new Author("asdfaa","1",0, R.drawable.music));
        autores2.add(new Author("aaa","2",0, R.drawable.music));
        autores3.add(new Author("bbbsadf","1",0, R.drawable.music));
        autores3.add(new Author("asdf","2",0, R.drawable.music));
        autores4.add(new Author("cdddarlos","1",0, R.drawable.music));
        autores4.add(new Author("sadfasfsadfasdf","2",0, R.drawable.music));

    }

    public static String [] votacion={"1","2","3","4","5"};
    public static String key="key";
}
