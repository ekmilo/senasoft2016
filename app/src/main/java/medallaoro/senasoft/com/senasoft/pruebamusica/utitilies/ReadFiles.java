package medallaoro.senasoft.com.senasoft.pruebamusica.utitilies;

import android.content.Context;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by ekmil on 13/7/2016.
 */
public class ReadFiles {
    public static List<String> readFile(Context context,String nameFile){
        BufferedReader reader = null;
        List<String> listFile= new ArrayList<>();
        try {
            reader = new BufferedReader(
                    new InputStreamReader(context.getAssets().open(nameFile)));

            String mLine;
            while ((mLine = reader.readLine()) != null) {
                listFile.add(mLine);
            }
        } catch (IOException e) {
        } finally {
            if (reader != null) {
                try {
                    reader.close();
                } catch (IOException e) {
                }
            }
        }
        return listFile;
    }
}
